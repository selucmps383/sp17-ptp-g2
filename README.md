# Entity and SQL #

Quick Overview

### Code First ###

* Entity Framework allows for a domain driven design called code first. It allows you to create classes for your domain requirement rather than design your database first and then make your classes around the database.
* Previously, using the Designer was the preferred method, but with Code First, you do exactly that, you use code instead of a designer.

![code-first.png](https://bitbucket.org/repo/Lq9krp/images/558926147-code-first.png)

This process gives you full control over the code(No Auto Generated Code), but you then rely on entity framework, and any problems will be more difficult to fix than if done with database first.

Code first uses a pattern known convention over configuration. What this means is that code first will guess that your classes follow the conventions that EF uses. If your classes do not follow those conventions, you have the ability to add configurations to your classes to provide EF with the information it needs.

### Data Annotations ###

* In Code first there are two methods of implementing configurations, one of these methods are adding simple attributes called Data Annotations. On top of making your classes compatible with Entity Framework they can also be used to add more information and detail to Entity Framework about the classes and the database they map to.

![image_thumb[1].png](https://bitbucket.org/repo/Lq9krp/images/549765977-image_thumb%5B1%5D.png)

![image_thumb[6].png](https://bitbucket.org/repo/Lq9krp/images/994431108-image_thumb%5B6%5D.png)


### EntityType Configurations ###

* The other method of implementing configurations is called Fluent API. EntityType Configurations is an important class that is inside of the Fluent API. It supplies you with many important methods to configure entities and override numerous code first conventions. You can access Entity<TEntity>() method of DbModelBuilder class as shown below. 

![ETC-fg1.PNG](https://bitbucket.org/repo/Lq9krp/images/2399388837-ETC-fg1.PNG)

### Data Contexts ###

Data Context allows for the integration of LINQ framework to SQL. 
>With Data Context you can:

* Connect to a database
* Access data
* Submit changes back to the server

The DatabaseAttribute shown in the above code fragment tells the compiler to link the DataContext to the Northwind database on the server.

Entity classes that contain a mapping between the code in your program and the data in your database, and then declare each field of the table by type, and declares whether or not it is nullable.
The DataContext provides a simple means of accessing individual instances of a class through a collection.
The DataContext also provides a means of connecting and opening a database.

### Migrations via PackageManager Console ###
Code-First has two commands for code based migration:  
1. Add-migration: It will scaffold the next migration for the changes you have made to your domain classes.  
2. Update-database: It will apply pending changes to the database based on latest scaffolding code file you created using "Add-Migration" command.  
  
You need to create scaffold file, which is a file that consists of the database requirements from the existing classes in your domain. You can do this by running the “add-migration" command in the package manager. You will have to pass the name parameter, which will be part of the code file name.

After creating the file above using the add-migration command, you have to update the database. You can create or update the database using the “update-database” command.  
At this point, the database will be created or updated.  

### SQL Statements and Queries ###

* It is language to communicate with a database.in other words it used to manage a data base.

Like in any programing language you have syntax and rules you must use to communicate properly with a database.  (A database most often contains one or more tables. Each table is identified by a name (e.g. "Customers" or "Orders"). Tables contain records (rows) with data.) –w3schools.com
 Theses sql commands are some of most important commands and a great start off point. 
SQL Commands
* SELECT
* INSERT
* UPDATE
* DELETE
* JOIN
* WHERE
Will go through each one and see how the work 

You have your select statement and a from statement to start with  
You have a customer table that you are trying to retrieve information from you will use the select command to get information back 
	
Example below 
SELECT column_name, column_name
FROM table_name;
Or 
SELECT * from table name ( select all)

The table  
![pic1.png](https://bitbucket.org/repo/Lq9krp/images/3313398039-pic1.png)  
The statement  
SELECT CustomerName,City FROM Customers;  
![Pic2.png](https://bitbucket.org/repo/Lq9krp/images/713310953-Pic2.png)  
Result is all names of the customers and the city they live in.

Test for yourself! (http://www.w3schools.com/sql/sql_select.asp)

INSERT command 
INSERT command allows you to insert new records in the existing database 

Two ways to insert 
INSERT INTO table_name
VALUES (value1,value2,value3,...);
Or 
INSERT INTO table_name (column1,column2,column3,...) (inserting directly to the columns and adding the values.) 
VALUES (value1,value2,value3,...);

Table Below
![pic3.png](https://bitbucket.org/repo/Lq9krp/images/3509101057-pic3.png)

The statement
 INSERT INTO Customers (CustomerName, ContactName, Address, City, PostalCode, Country)
 VALUES ('Cardinal','Tom B. Erichsen','Skagen 21','Stavanger','4006','Norway');

The Result  
![Pic4.png](https://bitbucket.org/repo/Lq9krp/images/3417177642-Pic4.png)


Test for yourself! (http://www.w3schools.com/sql/sql_insert.asp)

Update 
Is used to update the records on a database as well as the delete is used to delete the a record from the database 
UPDATE table_name
SET column1=value1,column2=value2,...
WHERE some_column=some_value;

And also 

DELETE FROM table_name
WHERE some_column=some_value;



Both will update the table.


Test for yourself! 
Update (http://www.w3schools.com/sql/sql_update.asp)
Delete (http://www.w3schools.com/sql/sql_delete.asp)

Where  
The WHERE clause is used to extract only those records that fulfill a specified criterion. 

Example 
SELECT column_name,column_name
FROM table_name
WHERE column_name operator value;  

Example table and result  
![pics5.png](https://bitbucket.org/repo/Lq9krp/images/2230966942-pics5.png)

SELECT * FROM Customers
WHERE Country='Mexico';  

![Pic2ElectricBoogaloo.png](https://bitbucket.org/repo/Lq9krp/images/1673677940-Pic2ElectricBoogaloo.png)

Try it out! (http://www.w3schools.com/sql/sql_where.asp)

### Practical Examples of SQL ###

It is usefull because it allows users to   

* Query
* Manipulate
* Define and Control Data in RDBMS.   

SQL queries makes it easier for programmers dealing with complex database relationships. SQL queries are used to access data from a particular database in RDBMS.

SQL databases can be used in Oracle and non-Oracle databases with minor syntax adjustments.

Also useful to make SQL reports and data analysis. 

Famous websites that use SQL  
* Flickr uses MySQL  
* Youtube uses MySQL  
* Myspace uses SQL Server  
* Wikipedia uses MySQL.   
 

### Object Relational Mapping (ORM) ###

* Technique for converting data stored in relational databases (SQL server, oracle) to domain-specific classes and back again
* Reduces writing redundant CRUD code


### Sources ###

* Migrations Via PackageManager: http://www.entityframeworktutorial.net/code-first/code-based-migration-in-code-first.aspx  
* Data Context: https://blogs.msdn.microsoft.com/charlie/2007/12/11/understanding-the-datacontext/  
* EntityTypeConfiguration: http://www.entityframeworktutorial.net/code-first/entitytypeconfiguration-class.aspx  
* Data Annotations: https://msdn.microsoft.com/en-us/library/jj591583(v=vs.113).aspx  
* Code First: http://www.entityframeworktutorial.net/code-first/simple-code-first-example.aspx